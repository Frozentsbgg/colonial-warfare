// Subsystem defines.
// All in one file so it's easier to see what everything is relative to.

#define SS_INIT_TICKER_SPAWN       999
#define SS_INIT_CHAT			   27
#define SS_INIT_RUST               26
#define SS_INIT_SUPPLY_SHUTTLE     25
#define SS_INIT_GARBAGE            23
#define SS_INIT_JOB                22
#define SS_INIT_PLANT              21.5
#define SS_INIT_HUMANS             21
#define SS_INIT_MAP                20
#define SS_INIT_COMPONENT          19.5
#define SS_INIT_POWER              19
#define SS_INIT_OBJECT             18
#define SS_INIT_DECORATOR          17.9
#define SS_INIT_PIPENET            17.5
#define SS_INIT_XENOARCH           17
#define SS_INIT_MORE_INIT          16
#define SS_INIT_AIR                15
#define SS_INIT_SHUTTLE            14
#define SS_INIT_TELEPORTER         13
#define SS_INIT_LIGHTING           12
#define SS_INIT_LANDMARK           11
#define SS_INIT_MAPVIEW            10
#define SS_INIT_DEFCON             9
#define SS_INIT_LAW                6
#define SS_INIT_FZ_TRANSITIONS     5
#define SS_INIT_ATOMS              3
#define SS_INIT_TIMER              1
#define SS_INIT_UNSPECIFIED        0
#define SS_INIT_EMERGENCY_SHUTTLE -19
#define SS_INIT_ASSETS            -20
#define SS_INIT_TICKER            -21
#define SS_INIT_FINISH            -22
#define SS_INIT_MINIMAP           -23
#define SS_INIT_ADMIN             -24
#define SS_INIT_DATABASE		  -25
#define SS_INIT_ENTITYMANAGER	  -26
#define SS_INIT_PLAYTIME    	  -27
#define SS_INIT_CORPSESPAWNER      -30


#define SS_PRIORITY_CHAT		   300
#define SS_PRIORITY_SOUND          250
#define SS_PRIORITY_TICKER         200
#define SS_PRIORITY_MAPVIEW		   170
#define SS_PRIORITY_QUADTREE       150
#define SS_PRIORITY_CELLAUTO       150
#define SS_PRIORITY_MOB            150
#define SS_PRIORITY_XENO           149
#define SS_PRIORITY_HUMAN          148
#define SS_PRIORITY_STAMINA        126
#define SS_PRIORITY_COMPONENT      125
#define SS_PRIORITY_NANOUI         120
#define SS_PRIORITY_VOTE           110
#define SS_PRIORITY_FAST_OBJECTS   105
#define SS_PRIORITY_OBJECTS        100
#define SS_PRIORITY_DECORATOR	   99
#define SS_PRIORITY_POWER          95
#define SS_PRIORITY_MACHINERY      90
#define SS_PRIORITY_FZ_TRANSITIONS 88
#define SS_PRIORITY_PIPENET        85
#define SS_PRIORITY_SHUTTLE        80
#define SS_PRIORITY_TELEPORTER     75
#define SS_PRIORITY_EVENT          65
#define SS_PRIORITY_DISEASE        60
#define SS_PRIORITY_FAST_MACHINERY 55
#define SS_PRIORITY_MIDI       	   40
#define SS_PRIORITY_ENTITY	       37
#define SS_PRIORITY_DEFCON         35
#define SS_PRIORITY_UNSPECIFIED    30
#define SS_PRIORITY_ROUND_RECORDING 83
#define SS_PRIORITY_SOUNDSCAPE	   24
#define SS_PRIORITY_LIGHTING       20
#define SS_PRIORITY_TRACKING       19
#define SS_PRIORITY_PING       	   10
#define SS_PRIORITY_DATABASE	   15
#define SS_PRIORITY_PLAYTIME       5
#define SS_PRIORITY_PERFLOGGING    4
#define SS_PRIORITY_CORPSESPAWNER  3
#define SS_PRIORITY_GARBAGE        2
#define SS_PRIORITY_INACTIVITY     1
#define SS_PRIORITY_ADMIN          0

#define SS_WAIT_MACHINERY           3.5 SECONDS //TODO move the rest of these to defines
#define SS_WAIT_FAST_MACHINERY      0.7 SECONDS
#define SS_WAIT_FAST_OBJECTS        0.5 SECONDS
#define SS_WAIT_CELLAUTO            0.05 SECONDS
#define SS_WAIT_ADMIN               5 MINUTES
#define SS_WAIT_FZ_TRANSITIONS		1 SECONDS
#define SS_WAIT_ROUND_RECORDING        5 SECONDS


#define SS_DISPLAY_GARBAGE        -100
#define SS_DISPLAY_DATABASE       -99
#define SS_DISPLAY_ENTITY	      -98
#define SS_DISPLAY_LIGHTING       -80
#define SS_DISPLAY_MOB            -72
#define SS_DISPLAY_HUMAN          -71
#define SS_DISPLAY_XENO           -70
#define SS_DISPLAY_COMPONENT      -69
#define SS_DISPLAY_FAST_OBJECTS   -65
#define SS_DISPLAY_DECORATOR	  -63
#define SS_DISPLAY_OBJECTS        -60
#define SS_DISPLAY_TIMER          -55
#define SS_DISPLAY_MACHINERY      -50
#define SS_DISPLAY_PIPENET        -40
#define SS_DISPLAY_FAST_MACHINERY -30
#define SS_DISPLAY_SHUTTLES       -25
#define SS_DISPLAY_TELEPORTER     -24
#define SS_DISPLAY_POWER          -20
#define SS_DISPLAY_FZ_TRANSITIONS -15
#define SS_DISPLAY_TICKER         -10
#define SS_DISPLAY_UNSPECIFIED     0
#define SS_DISPLAY_ADMIN           20

#define INITIALIZE_HINT_NORMAL   0  //Nothing happens
#define INITIALIZE_HINT_LATELOAD 1  //Call InitializeLate
#define INITIALIZE_HINT_QDEL     2  //Call qdel on the atom